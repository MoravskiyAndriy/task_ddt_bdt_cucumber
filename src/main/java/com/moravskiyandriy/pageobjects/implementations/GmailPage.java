package com.moravskiyandriy.pageobjects.implementations;

import com.moravskiyandriy.pageobjects.AbstractPageObject;
import com.moravskiyandriy.utils.Waiter;
import com.moravskiyandriy.webelement.implementations.Button;
import com.moravskiyandriy.webelement.implementations.CheckBox;
import com.moravskiyandriy.webelement.implementations.Label;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GmailPage extends AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(GmailPage.class);
    @FindBy(xpath = "//a[@href='https://mail.google.com/mail/u/0/#inbox']")
    private Button incomingLettersButton;
    @FindBy(css = "td.oZ-x3.xY")
    private List<CheckBox> incomingLettersCheckboxes;
    @FindBy(css = "div.T-I.J-J5-Ji.nX.T-I-ax7.T-I-Js-Gs.mA")
    private Button deleteButton;
    @FindBy(xpath = "//div[@class='J-J5-Ji amH J-JN-I']/span")
    private Label letterQuantityLabel;

    public void clickIncomingLettersButton() {
        LOGGER.info("going to Incoming Letters");
        incomingLettersButton.click();
    }

    public void checkLettersForDeletion(int quantity) {
        LOGGER.info("checking Letters for deletion");
        for (int i = 0; i < quantity; i++) {
            incomingLettersCheckboxes.get(i).setCheckBoxSelected();
        }
    }

    public void clickDeleteButton() {
        LOGGER.info("clicking Delete Button");
        Waiter.waitForElementToBeClickable(deleteButton, Waiter.MEDIUM_WAIT);
        deleteButton.click();
    }

    public int countIncomingLetters() {
        LOGGER.info("getting letters' quantity");
        clickIncomingLettersButton();
        String labelText = letterQuantityLabel.getText().trim();
        String LettersNumber = labelText.substring(labelText.lastIndexOf(" ") + 1).trim();
        return Integer.parseInt(LettersNumber);
    }
}
