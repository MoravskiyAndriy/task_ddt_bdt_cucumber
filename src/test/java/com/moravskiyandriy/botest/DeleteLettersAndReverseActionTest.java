package com.moravskiyandriy.botest;

import com.moravskiyandriy.businessobjects.AuthenticationBO;
import com.moravskiyandriy.businessobjects.GmailBO;
import com.moravskiyandriy.dataparcer.CSVDataParcer;
import com.moravskiyandriy.utils.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Optional;
import java.util.Properties;

import static org.testng.Assert.assertEquals;

public class DeleteLettersAndReverseActionTest {
    private static final String DATAFILE_PATH = "./src/main/resources/UserData.csv";
    private static final Logger LOGGER = LogManager.getLogger(DeleteLettersAndReverseActionTest.class);
    private static final int DEFAULT_QUANTITY_OF_LETTERS_TO_DELETE = 3;
    private static final int FINAL_QUANTITY_OF_LETTERS_TO_DELETE = Optional.ofNullable(getProperties()
            .getProperty("QUANTITY_OF_LETTERS_TO_DELETE"))
            .map(Integer::valueOf)
            .orElse(DEFAULT_QUANTITY_OF_LETTERS_TO_DELETE);

    @DataProvider(parallel = true)
    private Iterator<Object[]> users() {
        return CSVDataParcer.readFile(DATAFILE_PATH);
    }

    @Test(dataProvider = "users")
    void deleteLettersAndReverseAction(String accountAddress, String accountPassword) {
        int lettersQuantityBeforeDeletion;
        int lettersQuantityAfterDeletion;
        AuthenticationBO authenticationBO = new AuthenticationBO();
        GmailBO gmailBO = new GmailBO();
        authenticationBO.loginIntoGmail(accountAddress, accountPassword);
        lettersQuantityBeforeDeletion = gmailBO.goToIncomingAndCountLetters();
        gmailBO.chooseLettersDeleteUndo(FINAL_QUANTITY_OF_LETTERS_TO_DELETE);
        lettersQuantityAfterDeletion = gmailBO.goToIncomingAndCountLetters();
        assertEquals(lettersQuantityAfterDeletion, lettersQuantityBeforeDeletion);
    }

    @AfterMethod
    void quitDriver() {
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = DeleteLettersAndReverseActionTest.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            LOGGER.warn("NumberFormatException found.");
        } catch (IOException ex) {
            LOGGER.warn("IOException found.");
        }
        return prop;
    }
}
